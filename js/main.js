let dataGlasses = [
  {
    id: "G1",
    src: "./img/g1.jpg",
    virtualImg: "./img/v1.png",
    brand: "Armani Exchange",
    name: "Bamboo wood",
    color: "Brown",
    price: 150,
    description:
      "Lorem ipsum dolor sit amet consectetur adipisicing elit. Nobis ea voluptates officiis? ",
  },
  {
    id: "G2",
    src: "./img/g2.jpg",
    virtualImg: "./img/v2.png",
    brand: "Arnette",
    name: "American flag",
    color: "American flag",
    price: 150,
    description:
      "Lorem ipsum dolor, sit amet consectetur adipisicing elit. In assumenda earum eaque doloremque, tempore distinctio.",
  },
  {
    id: "G3",
    src: "./img/g3.jpg",
    virtualImg: "./img/v3.png",
    brand: "Burberry",
    name: "Belt of Hippolyte",
    color: "Blue",
    price: 100,
    description: "Lorem ipsum dolor, sit amet consectetur adipisicing elit.",
  },
  {
    id: "G4",
    src: "./img/g4.jpg",
    virtualImg: "./img/v4.png",
    brand: "Coarch",
    name: "Cretan Bull",
    color: "Red",
    price: 100,
    description: "In assumenda earum eaque doloremque, tempore distinctio.",
  },
  {
    id: "G5",
    src: "./img/g5.jpg",
    virtualImg: "./img/v5.png",
    brand: "D&G",
    name: "JOYRIDE",
    color: "Gold",
    price: 180,
    description:
      "Lorem ipsum dolor, sit amet consectetur adipisicing elit. Error odio minima sit labore optio officia?",
  },
  {
    id: "G6",
    src: "./img/g6.jpg",
    virtualImg: "./img/v6.png",
    brand: "Polo",
    name: "NATTY ICE",
    color: "Blue, White",
    price: 120,
    description: "Lorem ipsum dolor, sit amet consectetur adipisicing elit.",
  },
  {
    id: "G7",
    src: "./img/g7.jpg",
    virtualImg: "./img/v7.png",
    brand: "Ralph",
    name: "TORTOISE",
    color: "Black, Yellow",
    price: 120,
    description:
      "Lorem ipsum dolor sit amet consectetur adipisicing elit. Enim sint nobis incidunt non voluptate quibusdam.",
  },
  {
    id: "G8",
    src: "./img/g8.jpg",
    virtualImg: "./img/v8.png",
    brand: "Polo",
    name: "NATTY ICE",
    color: "Red, Black",
    price: 120,
    description:
      "Lorem ipsum dolor sit amet consectetur adipisicing elit. Reprehenderit, unde enim.",
  },
  {
    id: "G9",
    src: "./img/g9.jpg",
    virtualImg: "./img/v9.png",
    brand: "Coarch",
    name: "MIDNIGHT VIXEN REMIX",
    color: "Blue, Black",
    price: 120,
    description:
      "Lorem ipsum dolor sit amet consectetur adipisicing elit. Sit consequatur soluta ad aut laborum amet.",
  },
];

// document.getElementById("glassesList").
let glassList = "";
dataGlasses.forEach((glass) => {
  glassList += ` <img class="glasses__items col-4" src="${glass.src}" id="${glass.id}" alt="">`;
});
console.log(dataGlasses[0]);
document.getElementById("glassesList").innerHTML = glassList;
let glassTryOnButton = document.getElementsByClassName("glasses__items");
for (let index = 0; index < glassTryOnButton.length; index++) {
  glassTryOnButton[index].addEventListener("click", function () {
    tryOnGlass(dataGlasses[index]);
  });
}

let tryOnGlass = (data) => {
  let content = `
  <h3>${data.name} - ${data.brand} - (${data.color})</h3>
  <p><span class="text-success"><span class="badge badge-danger">$${data.price}</span> Stocking</span></p>
  <p>${data.description}</p>
  `;
  let img = `<img src="${data.virtualImg}" alt="" id="dataImg">`;

  document.getElementById("avatar").innerHTML = img;
  document.getElementById("glassesInfo").innerHTML = content;
  document.getElementById("glassesInfo").style.display = "block";
  //   glassesInfo
};

let removeGlasses = (condition) => {
  if (condition == true) {
    document.getElementById("dataImg").style.display = "block";
  } else if (condition == false) {
    document.getElementById("dataImg").style.display = "none";
    document.getElementById("glassesInfo").style.display = "none";
  }
};
